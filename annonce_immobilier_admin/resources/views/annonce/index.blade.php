
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <title>index</title>
    <style>
        .red-bg {
  background-color: red;
  padding: 5px;
  border-radius: 25%;
}
    </style>
</head>
<body>
    
<h1>liste des annonces</h1>
<!-- @if(session('success')) -->
    <!-- <div class="alert alert-success">{{ session('success') }}</div>
    <script>
        setTimeout(function(){
            $('.alert-success').fadeOut();
        }, 3000);
    </script> -->
<!-- @endif -->
@if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            
        @endif
<a href="{{route('annonce.create')}}"  class="btn btn-primary">Nouvelle annonce</a>
@isset($annonces)
<table class="table mt-3">
<tr>
    <th>Titre</th>
    <th>Description</th>
    <th>Type</th>
    <th>Ville</th>
    <th>Superficie</th>
    <th>etat</th>
    <th>Prix(dhs)</th>
    <th>Action</th>
</tr>
@foreach($annonces as $annonce )
    <tr>
        <td>{{$annonce->titre}}</td>
        <td>{{$annonce->description}}</td>
        <td>{{$annonce->type}}</td>
        <td>{{$annonce->ville}}</td>
        <td>{{$annonce->superficie}}</td>
        <td>{{$annonce->neuf}}</td>
        <td>{{$annonce->prix}}</td>
        <td>
        <form method="post" action="{{route('annonce.destroy', $annonce->id)}}"  onsubmit="return confirm('Voulez-vous vraiment supprimer cette annonce ?');">
								@csrf
								@method('DELETE')
								<a href="{{ route('annonce.show', $annonce->id) }}" class="btn btn-light btn-sm "><i class="bi bi-list"></i></a>
								<a href="{{ route('annonce.edit', $annonce->id) }}" class="btn btn-light btn-sm bg-success"><i class="bi bi-pencil"></i></a>
								<!-- <input type="submit" class="bi bi-trash3-fill" /> -->

                                <button type="submit" style="border: none; " class="red-bg"><i class="bi bi-trash"></i></button>
							</form>
        </td>
    </tr>
    @endforeach
</table>
@endisset
</body>

</html>