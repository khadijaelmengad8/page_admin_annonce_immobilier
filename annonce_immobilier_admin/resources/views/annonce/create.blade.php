<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    
    <title>index</title>
</head>
<body>
<h1>Nouvelle annonce</h1>
<form action="{{route('annonce.store')}}" method="post" class="col-8">
    @csrf
    <div class="mb-3">
            <label class="form-label" for="titre">Titre</label>
            <input type="text" class="form-control" name="titre" id="titre">
        </div>
          <div class="mb-3">
            <label class="form-label" for="description">Description</label>
            <input type="text" class="form-control" name="description" id="description">
        </div>
          <div class="mb-3">
            <label class="form-label" for="type">Type</label>
            <select name="type" class="form-select">
              <option value="Appartement">Appartement</option>
              <option value="Maison">Maison</option>
              <option value="Villa">Villa</option>
              <option value="Magasin">Magasin</option>
              <option value="Terrain">Terrain</option>
            </select>
        </div>
          <div class="mb-3">
            <label class="form-label" for="ville">Ville</label>
            <input type="text" class="form-control" name="ville" id="ville">
        </div>
          <div class="mb-3">
            <label class="form-label" for="superficie">Superficie</label>
            <input type="text" class="form-control" name="superficie" id="superficie">
        </div>
          <div class="mb-3">
            <label class="form-label" for="prix">Prix</label>
            <input type="text" class="form-control" name="prix" id="prix">
        </div>
        <input type="submit" value="Ajouter" class="btn btn-primary">
    
</form>
</body>
</html>