<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
 
    <title>Document</title>
</head>
<body>
<div class="container mt-4 w-50">
        <h2> Détails</h2>
        <div class="form-group">
           <strong>Titre :</strong>
           {{ $annonce->titre}}
        </div>
        <div class="form-group">
            <strong>Description:</strong>
            {{ $annonce->description }}
        </div>
        <div class="form-group">
            <strong>type :</strong>
            {{ $annonce->type }}
        </div>
        <div class="form-group">
            <strong>ville :</strong>
            {{ $annonce->ville }}
        </div>
        <div class="form-group">
            <strong>superficie :</strong>
            {{ $annonce->superficie }}
        </div>
        
       </div>
 
</body>
</html>