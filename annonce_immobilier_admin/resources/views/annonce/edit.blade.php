<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Modification</title>
</head>
 
<body class="container mt-2">
<h2>Modification de l'annonce</h2>
<form action="{{route('annonce.update',$annonce->id)}}" method="post" class="col-8">
    @csrf
    @method('PUT')
    <div class="mb-3">
            <label class="form-label" for="titre">Titre</label>
            <input type="text" class="form-control" name="titre" id="titre" value="{{$annonce->titre}}">
        </div>
          <div class="mb-3">
            <label class="form-label" for="description">Description</label>
            <input type="text" class="form-control" name="description" id="description" value="{{$annonce->description}}" />
        </div>
          <div class="mb-3">
            <label class="form-label" for="type">Type</label>
            <input type="text" class="form-control" name="type" id="type" value="{{$annonce->type}}"/>
        </div>
          <div class="mb-3">
            <label class="form-label" for="ville">Ville</label>
            <input type="text" class="form-control" name="ville" id="ville"  value="{{$annonce->ville}}"/>
        </div>
          <div class="mb-3">
            <label class="form-label" for="superficie">Superficie</label>
            <input type="text" class="form-control" name="superficie" id="superficie"  value="{{$annonce->superficie}}"/> 
        </div>
          <div class="mb-3">
            <label class="form-label" for="prix">Prix</label>
            <input type="text" class="form-control" name="prix" id="prix" value="{{$annonce->prix}}"/>
        </div>
  <button type="submit" class="btn btn-primary">Enregistrer</button>
    
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

</body>
</html>