<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class AnnonceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        db::table("annonces")->insert(
            [
            
                'titre'=> "vente d'une maison R+2",
                'description'=> "une jolie maison est en vente a hay saada",
                'type'=>"Maison",
                'ville'=>"fes",
                'superficie'=>'120',
                'neuf'=>true,
                'prix'=>1800000.00
          
            ]
            );
    }
}
