<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use Illuminate\Http\Request;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $annonces=Annonce::all();
        return view('annonce.index',compact("annonces"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('annonce.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $annonce=Annonce::create([
            "titre"=>$request->titre,
            "description"=>$request->description,       
            "type"=>$request->type,       
            "ville"=>$request->ville,       
            "superficie"=>$request->superficie,        
            "prix"=>$request->prix,        
        ]);
        return redirect()->route('annonce.index')->with("succes","l'annonce a ete ajoute !");
    }

    /**
     * Display the specified resource.
     */
    public function show(Annonce $annonce)
    {
        return view("annonce.show", compact("annonce"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Annonce $annonce)
    {
        return view("annonce.edit", compact("annonce"));
   
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Annonce $annonce)
    {
        $annonce->update($request->all());
        return redirect()->route('annonce.index')->with('success','Le stagiaire a été modifié avec succès.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Annonce $annonce)
    {
        $annonce->delete();
        return redirect()->route('annonce.index')->with('success','Annonce supprimée avec succès !');
    }
}
