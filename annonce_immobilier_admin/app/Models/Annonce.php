<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
    
    const types = ['Appartement', 'Maison', 'Villa', 'Magasin', 'Terrain'];
    protected $guarded=[];
    use HasFactory;
}
